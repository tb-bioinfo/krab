# KrAb

*KrAb* (**Kr**aken to **Ab**undance plot) is a program to display abundances compared to expected values from various *Kraken 2* reports.

## Table of Contents

- [Description](#description)
- [Dependencies](#dependencies)
- [Configuration](#configuration)
- [Usage](#usage)

## Description

*KrAb* takes a set of [*Kraken 2*](https://github.com/DerrickWood/kraken2) reports and displays the abundances compared to given expected values. The list of taxa to be displayed have to be given in a configuration file.

The plot is generated in an HTML file and automatically opened.

## Dependencies

*KrAb* needs *Python* to run and the following modules:

- *yaml*
- *pandas*
- *bokeh*

You can install the modules with pip:

```
pip install pyyaml pandas bokeh
```

## Configuration

*KrAb* relies on three configuration files to work.

### config.yml

This file contains general configuration. It contains the following values:
- `default_tree_file`: The tree file to be used if none is specified.
- `expected_sample_name`: The name of the expected column displayed on the graph.
- `plot_title` *(optional)*: The title displayed on the graph
- `plot_xlabel` *(optional)*: The X axis label displayed on the graph
- `plot_ylabel` *(optional)*: The Y axis label displayed on the graph

### kraken-reports.yml

This file contains the information about the *Kraken 2* reports to be used. The reports have to be provided on this format, one per line:

`Name: /path/to/report/file.k2report`

`Name` is the name of the sample that will be displayed on the graph. The report file is the corresponding *Kraken 2* report.

### Tree information file

The tree information file contains the information about the taxa to be extracted from the *Kraken 2* report files. It is a *Python* file to put in the *trees* folder. A file containing the bacteria in the *Zymobiomics* mock community with the 16S gene proportions on the family, genus and species levels is provided (See https://files.zymoresearch.com/protocols/_d6300_zymobiomics_microbial_community_standard.pdf).

The file defines a `tree_information` variable containing the list of taxa. each taxa is a tuple containing the following information: `(color, expected_proportion, [taxonomic_rank, names_list, children_list])`.

- `color` is a string containing the color code used on the graph for this taxon. Example: `"#6666ff"`
- `expected_proportion` is the expected proportion of this taxon, displayed on the first column of the graph.
- `taxonomik_rank` is the taxonomic rank as displayed in the *Kraken 2* report. Examples: `F`, `G`, `S`.
- `names_list` is a list containing the name of the taxon and its synonym. If you use only one *Kraken 2* database, it should contain only one element. But if the different *Kraken 2* reports are made with different databases, the name can vary. The first name of the list will be displayed on the graph.
- `children_list` is the list of the children taxa of this taxon included in the tree. Each child is designed by a string corresponding to the first name of the `names_list` of the child. The leaves of the tree should have an empty list.

The first taxon of the file is used to count the total number of sequences, so it should include all the others.

## Usage

```
usage: krab.py [--tree TREEFILE]

optional arguments:
  -h, --help            Show this help message and exit
  -t, --tree TREEFILE   The tree information file without the extension. If not specified, the default_tree_file value in config.yml file will be used.
```
