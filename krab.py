import os
import sys
import yaml
import pandas as pd
from bokeh.io import save, output_file
from bokeh.models import ColumnDataSource
from bokeh.plotting import figure
from bokeh.core.properties import value
from bokeh.palettes import d3, brewer
import webbrowser
import itertools
import argparse
import importlib
import warnings

warnings.filterwarnings("ignore", category=DeprecationWarning)

CONFIG_FILE = "config.yml"
KRAKEN_REPORTS_FILE = "kraken-reports.yml"
TREES_DIRECTORY = "trees"

parser=argparse.ArgumentParser(description="""Program to display abundances compared to theorical values from Kraken 2 reports.""")

parser.add_argument('-t', '--tree', help='The tree information file without the extension. If not specified, the default_tree_file value in config.yml file will be used.', type=str)

args = parser.parse_args()

def usage():
    parser.print_help()

# Read configuration files
python_files_directory = os.path.dirname(os.path.abspath(__file__))

with open(os.path.abspath(os.path.join(python_files_directory, CONFIG_FILE)), 'r') as yaml_file:
    config = yaml.load(yaml_file, Loader=yaml.FullLoader)

with open(os.path.abspath(os.path.join(python_files_directory, KRAKEN_REPORTS_FILE)), 'r') as yaml_file:
    kraken_reports = yaml.load(yaml_file, Loader=yaml.FullLoader)

if args.tree is None:
    tree_file = config["default_tree_file"]
else:
    tree_file = args.tree

sys.path.append(os.path.abspath(os.path.join(python_files_directory, TREES_DIRECTORY)))
tree_information = importlib.import_module(tree_file).tree_information
colors = [info[0] for info in tree_information]
theoricals = [info[1] for info in tree_information]
tree = [info[2] for info in tree_information]

bacteria_list = [node[1][0] for node in tree]
indents = {'D':"", 'F':"  ", 'G':"    ", 'S':"      "} # For plot legend
data = {"taxa": [f"{indents[node[0]]}{node[1][0]}" for node in tree], config.get('expected_sample_name', "Expected"):theoricals}
data_rank = {}
html_data = {}

# Read Kraken reports
for db_name, kraken_file in kraken_reports.items():
    html_data[db_name] = {"kraken_report":kraken_file}
    print(f"\n=======>{db_name}")
    html_data[db_name]["total_reads"] = 0
    with open(kraken_file, 'r') as kraken_file:
        lines = kraken_file.readlines()

        counts = {}
        counts_rank = {rank:{} for rank in indents.keys()}
        total = 1
        for line in lines:
            sline = line.strip().split('\t')
            count = int(sline[1])
            level = sline[3]
            if level == 'U' or level == 'R':
                html_data[db_name]["total_reads"] += count # total number of reads in the whole Kraken report
            name = sline[5].strip()
            if level==tree[0][0] and name==tree[0][1][0]:
                total = count # total number of reads in the taxa of interest
            # Count number of reads in each clade
            for node in tree:
                if node[0]==level and name in node[1]:
                    counts[node[1][0]] = counts.get(node[1][0], 0) + count
        # Subtract included clades
        for node in tree:
            counts_rank[node[1][0]] = counts_rank.get(node[1][0], counts.get(node[1][0], 0)) + sum([counts_rank.get(child, 0) for child in node[2]])
            counts[node[1][0]] = counts.get(node[1][0], 0) - sum([counts.get(child, 0) for child in node[2]])
        # Compute ratio
        data[db_name] = [round(100*counts[taxon]/total, 2) for taxon in bacteria_list]
        data_rank[db_name] = [round(100*counts_rank[taxon]/total, 2) for taxon in bacteria_list]
        print(data[db_name])
        print(data_rank[db_name])
        html_data[db_name]["n_reads"] = total

# Compute total expected for each rank
counts_expected = {}
for index, node in reversed(list(enumerate(tree))):
    counts_expected[node[1][0]] = counts_expected.get(node[1][0], theoricals[index]) + sum([counts_expected.get(child, 0) for child in node[2]])
counts_expected = list(counts_expected.values())[::-1]
print('\n')
print([node[0] for node in tree])
print(counts_expected)
print(data_rank)

# Compute assignation success
ranks_list = [node[0] for node in tree]
for db_name, counts_assigned in data_rank.items():
    print(f"---> {db_name}")
    html_data[db_name]["percent_assigned"] = {}
    for rank in indents.keys():
        counts_assigned_rank = [count for count, rk in zip(counts_assigned, ranks_list) if rk==rank]
        counts_expected_rank = [count for count, rk in zip(counts_expected, ranks_list) if rk==rank]
        assigned_ratio = sum(counts_assigned_rank)
        counts_expected_rank.append(round(100.0-sum(counts_expected_rank), 2))
        counts_assigned_rank.append(round(100.0-assigned_ratio, 2))
        html_data[db_name]["percent_assigned"][rank] = round(assigned_ratio, 2)
        print(counts_expected_rank, counts_assigned_rank, '\n')

# Plot
df = pd.DataFrame(data)

output_file(f"abundance-{tree_file}.html", title=config.get('plot_title', f"Abundance {tree_file}"))

df.set_index('taxa', inplace=True)

samples = df.columns.values
organisms = df.index.values

data = {'samples': list(samples)}
for organism in organisms:
    data[organism] = list(df.loc[organism])
source = ColumnDataSource(data=data)

p = figure(x_range=samples, plot_height=600, title=config.get('plot_title', None),
           toolbar_location=None, tools="")
p.vbar_stack(organisms, x='samples', width=0.9, source=source,
             legend=[value(x) for x in organisms], color=colors)

p.xaxis.axis_label = config.get('plot_xlabel', None)
p.yaxis.axis_label = config.get('plot_ylabel', None)
p.legend.location = "top_right"
p.legend.orientation = "vertical"

p.y_range.flipped = True 

new_legend = p.legend[0]
p.add_layout(new_legend, 'right')

save(p)

# Right column information
def percentage_to_color(percentage):
    percentage = max(0, min(100, percentage))
    green = int(percentage*255/100)
    red = 255 - green
    color_code = f"#{red:02X}{green:02X}00"
    return color_code

right_div = ""

for db_name, db_data in html_data.items():
    right_div += f"""<h3>{db_name}</h3>
    {db_data["kraken_report"]}<br/>
    {db_data["n_reads"]}/{db_data["total_reads"]} reads<br/>"""

    for rank, percent in db_data["percent_assigned"].items():
        final_percent = round(percent*db_data["n_reads"]/db_data["total_reads"], 2)
        right_div += f"<b>{rank}:</b> <span class=\"circle\" style=\"background:{percentage_to_color(final_percent)}\"></span> {final_percent}%&nbsp;&nbsp;"

with open(f"abundance-{tree_file}.html", "r") as html_file:
    html = html_file.read()

html = html.replace("</head>", """<style>
        body {
            margin: 0;
            padding: 0;
            display: flex;
        }

        #leftDiv {
            padding: 20px;
        }

        #rightDiv {
            padding: 20px;
            flex: 1;
        }

        .circle {
            display: inline-block;
            width: 10px;
            height: 10px;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
        }
    </style>
  </head>""")
html = html.replace("<body>", """<body><div id="leftDiv">""")
html = html.replace("</body>", f"""</div>
    <div id="rightDiv">{right_div}</div></body>""")

with open(f"abundance-{tree_file}.html", "w") as html_file:
    html = html_file.write(html)
webbrowser.open('file://' + os.path.realpath(f"abundance-{tree_file}.html"))
